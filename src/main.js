// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from '@/store'
import firebase  from 'firebase'

Vue.config.productionTip = false

/* eslint-disable no-new */

let app
const initializeApp =() =>{
  if(!app){
    app = new Vue({
      router,
      store: store,
      render: h =>h(App)
    }).$mount('#app')
  }
}

firebase.auth().onAuthStateChanged(user =>{
  initializeApp();
  if(user){
    store.commit('setCurrentUser',user)
  }
  else{
    store.commit('setCurrentUser',null)
  }
  

})

