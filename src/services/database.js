import store from '@/store'
import firebase from 'firebase'

  var firebaseConfig = {
        apiKey: "AIzaSyCUZ1oy-qxnYj2X3DVKEeX8wQUc6xrJJks",
        authDomain: "vuejs-f7772.firebaseapp.com",
        databaseURL: "https://vuejs-f7772.firebaseio.com",
        projectId: "vuejs-f7772",
        storageBucket: "vuejs-f7772.appspot.com",
        messagingSenderId: "623151261094",
        appId: "1:623151261094:web:3b2d66a7a0227c3b031b67"
      };
    

    const database = firebase.initializeApp(firebaseConfig);

    database.signUp = async (email, password) =>{
        try{
            await firebase.auth().createUserWithEmailAndPassword(email, password)
            store.commit('setCurrentUser', firebase.auth().currentUser)
            return true

        } catch(error){
            return error
        }
    }

    database.signIn = async (email, password) =>{
        try{
            await firebase.auth().signInWithEmailAndPassword(email, password)
            store.commit('setCurrentUser', firebase.auth().currentUser)
            return true

        } catch(error){
            return error
        }
    }

    database.signOut = async () =>{
        try{
            await firebase.auth().signOut()
            store.commit('setCurrentUser', null)
            return true

        } catch(error){
            return error
        }
    }
    database.insertData =() =>{
        return database.database().ref("users");  
    }
  
    export default database;