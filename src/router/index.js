import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home.vue'
import Signup from '@/components/Signup.vue'
import Signin from '@/components/Signin.vue'
import Profile from '@/components/Profile.vue'
import store from '@/store'


Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup
    },
    {
      path: '/signin',
      name: 'signin',
      component: Signin
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      meta: {
        auth: true
      }
    }
  ],
  mode: "history"  //to avoid # at url it is necessary
})

router.beforeEach((to, from, next)=>{
  if(to.meta.auth && !store.state.currentUser){
    next({
      path: 'signin'
    })
  }else{
    next()
  }
})
export default router;
