import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);


export default new Vuex.Store({
    state: {                     //data of the application that needed to be stored, that will be available throughout the application
        currentUser : null
    },
    mutations:{   //should be call with commit,  this is always synchronous
        setCurrentUser(state, payload){
            state.currentUser = payload;   //payload may be user/null
        },
        hello(state, payload){
            state.currentUser = payload;   //payload may be user/null
        }

    },
    actions:{     //asynchronous or synchronous

    },
    getters:{  //instead of using computed, each component can get the information from here

    }
});



